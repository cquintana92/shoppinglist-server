module shoppinglistserver

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.10.4
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.0
	github.com/urfave/cli v1.22.4
)
